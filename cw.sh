#!/bin/sh

WALLPATH="/home/ethan/Pictures/Wallpaper"
PIC=$(ls $WALLPATH | dmenu -nb "#222222" -nf "#bbbbbb" -sb "#000000" -sf "#eeeeee")
FILEPATH="$WALLPATH/$PIC"
$(feh --bg-scale $FILEPATH 2> /dev/null)
