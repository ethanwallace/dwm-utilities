# Ethan's Utility Scripts

These scripts are utility scripts designed to suppliment my DWM install.
For my DWM startup scripts see
<https://gitlab.com/ethanwallace/dwm-scripts>. 

## Installing these scripts

These scripts are self contained units, and one may pick and choose
between them. To install a given script, create a symbolic link to it
from your local binary folder like so:

`sudo ln -s path/to/script.sh /usr/local/bin`

## About `cw.sh`

*Requires:* `dmenu`, `feh`

This script uses feh to change the system wallpaper quickly. It spawns a
dmenu instance that allows you select the file you want to display. By
default, the script searches the xdg standard Pictures folder for a file
called `Wallpaper`, and displays any file inside. If you do not store you
wallpapers inside /home/user/Pictures/Wallpaper you need to change the
`WALLPATH` variable inside the script to point to where you store them.
You could also alter the dmenu colours there as well - be default it uses
a dark theme.
